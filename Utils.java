import java.util.*;

public abstract class Utils{
    //public static final String BLACK_BACKGROUND   = "\u001B[40m \u001B[0m";
    //public static final String PURPLE_BACKGROUND  = "\u001B[45m \u001B[0m";
    //public static final String CYAN_BACKGROUND    = "\u001B[46m \u001B[0m";
    public static final String RED_BACKGROUND       = "\u001B[41m  \u001B[0m";
    public static final String GREEN_BACKGROUND     = "\u001B[42m  \u001B[0m";
    public static final String YELLOW_BACKGROUND    = "\u001B[43m  \u001B[0m";
    public static final String BLUE_BACKGROUND      = "\u001B[44m  \u001B[0m";
    public static final String WHITE_BACKGROUND     = "\u001B[47m  \u001B[0m";
    public static final String ORANGE_BACKGROUND    = "\033[48;2;255;165m  \033[m"; //r=255 g=225 b=115
    
    static Scanner scanner = new Scanner(System.in);

    public static void InputColorsToFaces(Rubiks R){
        // Input Up Face Colors
        System.out.printf("Up Face Colors : ");
        for (int sticker = 0; sticker < R.UpFace.length; sticker++){
            boolean IsColorValid = false;
            while (!IsColorValid){
                try{
                    R.UpFace[sticker] = scanner.next().charAt(0);
                    IsColorValid = true;
                }
                catch (InputMismatchException e){
                    System.out.println("You can only input char!");
                }
            }
        }

        // Input Front Face Colors
        System.out.printf("Front Face Colors : ");
        for (int sticker = 0; sticker < R.FrontFace.length; sticker++){
            boolean IsColorValid = false;
            while (!IsColorValid){
                try{
                    R.FrontFace[sticker] = scanner.next().charAt(0);
                    IsColorValid = true;
                }
                catch (InputMismatchException e){
                    System.out.println("You can only input char!");
                }
            }
        }

        // Input Right Face Colors
        System.out.printf("Right Face Colors : ");
        for (int sticker = 0; sticker < R.RightFace.length; sticker++){
            boolean IsColorValid = false;
            while (!IsColorValid){
                try{
                    R.RightFace[sticker] = scanner.next().charAt(0);
                    IsColorValid = true;
                }
                catch (InputMismatchException e){
                    System.out.println("You can only input char!");
                }
            }
        }

        // Input Back Face Colors
        System.out.printf("Back Face Colors : ");
        for (int sticker = 0; sticker < R.BackFace.length; sticker++){
            boolean IsColorValid = false;
            while (!IsColorValid){
                try{
                    R.BackFace[sticker] = scanner.next().charAt(0);
                    IsColorValid = true;
                }
                catch (InputMismatchException e){
                    System.out.println("You can only input char!");
                }
            }
        }

        // Input Left Face Colors
        System.out.printf("Left Face Colors : ");
        for (int sticker = 0; sticker < R.LeftFace.length; sticker++){
            boolean IsColorValid = false;
            while (!IsColorValid){
                try{
                    R.LeftFace[sticker] = scanner.next().charAt(0);
                    IsColorValid = true;
                }
                catch (InputMismatchException e){
                    System.out.println("You can only input char!");
                }
            }
        }

        // Input Down Face Colors
        System.out.printf("Down Face Colors : ");
        for (int sticker = 0; sticker < R.DownFace.length; sticker++){
            boolean IsColorValid = false;
            while (!IsColorValid){
                try{
                    R.DownFace[sticker] = scanner.next().charAt(0);
                    IsColorValid = true;
                }
                catch (InputMismatchException e){
                    System.out.println("You can only input char!");
                }
            }
        }
    }

    public static String[][] GetColors(Rubiks R){
        String[][] StickerColors = new String[6][9];
        // UpFace Sticker Colors
        for (int Sticker = 0; Sticker < 9; Sticker++){
            switch (R.UpFace[Sticker]){
                case 'y' :
                    StickerColors[0][Sticker] = YELLOW_BACKGROUND;
                    break;
                case 'r' :
                    StickerColors[0][Sticker] = RED_BACKGROUND;
                    break;
                case 'g' :
                    StickerColors[0][Sticker] = GREEN_BACKGROUND;
                    break;
                case 'o' :
                    StickerColors[0][Sticker] = ORANGE_BACKGROUND;
                    break;
                case 'b' :
                    StickerColors[0][Sticker] = BLUE_BACKGROUND;
                    break;
                case 'w' :
                    StickerColors[0][Sticker] = WHITE_BACKGROUND;
                    break;
            }
        }
        // FrontFace Sticker Colors
        for (int Sticker = 0; Sticker < 9; Sticker++){
            switch (R.FrontFace[Sticker]){
                case 'y' :
                    StickerColors[1][Sticker] = YELLOW_BACKGROUND;
                    break;
                case 'r' :
                    StickerColors[1][Sticker] = RED_BACKGROUND;
                    break;
                case 'g' :
                    StickerColors[1][Sticker] = GREEN_BACKGROUND;
                    break;
                case 'o' :
                    StickerColors[1][Sticker] = ORANGE_BACKGROUND;
                    break;
                case 'b' :
                    StickerColors[1][Sticker] = BLUE_BACKGROUND;
                    break;
                case 'w' :
                    StickerColors[1][Sticker] = WHITE_BACKGROUND;
                    break;
            }
        }
        // RightFace Sticker Colors
        for (int Sticker = 0; Sticker < 9; Sticker++){
            switch (R.RightFace[Sticker]){
                case 'y' :
                    StickerColors[2][Sticker] = YELLOW_BACKGROUND;
                    break;
                case 'r' :
                    StickerColors[2][Sticker] = RED_BACKGROUND;
                    break;
                case 'g' :
                    StickerColors[2][Sticker] = GREEN_BACKGROUND;
                    break;
                case 'o' :
                    StickerColors[2][Sticker] = ORANGE_BACKGROUND;
                    break;
                case 'b' :
                    StickerColors[2][Sticker] = BLUE_BACKGROUND;
                    break;
                case 'w' :
                    StickerColors[2][Sticker] = WHITE_BACKGROUND;
                    break;
            }
        }
        // BackFace Sticker Colors
        for (int Sticker = 0; Sticker < 9; Sticker++){
            switch (R.BackFace[Sticker]){
                case 'y' :
                    StickerColors[3][Sticker] = YELLOW_BACKGROUND;
                    break;
                case 'r' :
                    StickerColors[3][Sticker] = RED_BACKGROUND;
                    break;
                case 'g' :
                    StickerColors[3][Sticker] = GREEN_BACKGROUND;
                    break;
                case 'o' :
                    StickerColors[3][Sticker] = ORANGE_BACKGROUND;
                    break;
                case 'b' :
                    StickerColors[3][Sticker] = BLUE_BACKGROUND;
                    break;
                case 'w' :
                    StickerColors[3][Sticker] = WHITE_BACKGROUND;
                    break;
            }
        }
        // LeftFace Sticker Colors
        for (int Sticker = 0; Sticker < 9; Sticker++){
            switch (R.LeftFace[Sticker]){
                case 'y' :
                    StickerColors[4][Sticker] = YELLOW_BACKGROUND;
                    break;
                case 'r' :
                    StickerColors[4][Sticker] = RED_BACKGROUND;
                    break;
                case 'g' :
                    StickerColors[4][Sticker] = GREEN_BACKGROUND;
                    break;
                case 'o' :
                    StickerColors[4][Sticker] = ORANGE_BACKGROUND;
                    break;
                case 'b' :
                    StickerColors[4][Sticker] = BLUE_BACKGROUND;
                    break;
                case 'w' :
                    StickerColors[4][Sticker] = WHITE_BACKGROUND;
                    break;
            }
        }
        // DownFace Sticker Colors
        for (int Sticker = 0; Sticker < 9; Sticker++){
            switch (R.DownFace[Sticker]){
                case 'y' :
                    StickerColors[5][Sticker] = YELLOW_BACKGROUND;
                    break;
                case 'r' :
                    StickerColors[5][Sticker] = RED_BACKGROUND;
                    break;
                case 'g' :
                    StickerColors[5][Sticker] = GREEN_BACKGROUND;
                    break;
                case 'o' :
                    StickerColors[5][Sticker] = ORANGE_BACKGROUND;
                    break;
                case 'b' :
                    StickerColors[5][Sticker] = BLUE_BACKGROUND;
                    break;
                case 'w' :
                    StickerColors[5][Sticker] = WHITE_BACKGROUND;
                    break;
            }
        }
        return StickerColors;
    }

    public static void DisplayFaces(Rubiks R){
        System.out.println("Up Face : ");
        System.out.printf(R.UpFace[7] + " " + R.UpFace[0] + " " + R.UpFace[1] + "\n");
        System.out.printf(R.UpFace[6] + " " + R.UpFace[8] + " " + R.UpFace[2] + "\n");
        System.out.printf(R.UpFace[5] + " " + R.UpFace[4] + " " + R.UpFace[3] + "\n\n");

        System.out.println("Front Face : ");
        System.out.printf(R.FrontFace[7] + " " + R.FrontFace[0] + " " + R.FrontFace[1] + "\n");
        System.out.printf(R.FrontFace[6] + " " + R.FrontFace[8] + " " + R.FrontFace[2] + "\n");
        System.out.printf(R.FrontFace[5] + " " + R.FrontFace[4] + " " + R.FrontFace[3] + "\n\n");

        System.out.println("Right Face : ");
        System.out.printf(R.RightFace[7] + " " + R.RightFace[0] + " " + R.RightFace[1] + "\n");
        System.out.printf(R.RightFace[6] + " " + R.RightFace[8] + " " + R.RightFace[2] + "\n");
        System.out.printf(R.RightFace[5] + " " + R.RightFace[4] + " " + R.RightFace[3] + "\n\n");

        System.out.println("Back Face : ");
        System.out.printf(R.BackFace[7] + " " + R.BackFace[0] + " " + R.BackFace[1] + "\n");
        System.out.printf(R.BackFace[6] + " " + R.BackFace[8] + " " + R.BackFace[2] + "\n");
        System.out.printf(R.BackFace[5] + " " + R.BackFace[4] + " " + R.BackFace[3] + "\n\n");

        System.out.println("Left Face : ");
        System.out.printf(R.LeftFace[7] + " " + R.LeftFace[0] + " " + R.LeftFace[1] + "\n");
        System.out.printf(R.LeftFace[6] + " " + R.LeftFace[8] + " " + R.LeftFace[2] + "\n");
        System.out.printf(R.LeftFace[5] + " " + R.LeftFace[4] + " " + R.LeftFace[3] + "\n\n");

        System.out.println("Down Face : ");
        System.out.printf(R.DownFace[7] + " " + R.DownFace[0] + " " + R.DownFace[1] + "\n");
        System.out.printf(R.DownFace[6] + " " + R.DownFace[8] + " " + R.DownFace[2] + "\n");
        System.out.printf(R.DownFace[5] + " " + R.DownFace[4] + " " + R.DownFace[3] + "\n\n");
    }

    /* NANTI */
    public static void DisplayCubeNet(Rubiks R){
        String[][] StickerColors = GetColors(R);
        System.out.println("Cube Net :");
        System.out.println("\t\t   +----+----+----+");
        System.out.println("\t\t   | " + StickerColors[0][7] + " | " + StickerColors[0][0] + " | " + StickerColors[0][1] + " |");
        System.out.println("\t\t   |----+----+----|");
        System.out.println("\t\t   | " + StickerColors[0][6] + " | " + StickerColors[0][8] + " | " + StickerColors[0][2] + " |");
        System.out.println("\t\t   |----+----+----|");
        System.out.println("\t\t   | " + StickerColors[0][5] + " | " + StickerColors[0][4] + " | " + StickerColors[0][3] + " |");
        System.out.println("    +----+----+----+----+----+----+----+----+----+----+----+----|");
        System.out.println("    | " + StickerColors[4][7] + " | " + StickerColors[4][0] + " | " + StickerColors[4][1] + " | " + StickerColors[1][7] + " | " + StickerColors[1][0] + " | " + StickerColors[1][1] + " | " + StickerColors[2][7] + " | " + StickerColors[2][0] + " | " + StickerColors[2][1] + " | " + StickerColors[3][7] + " | " + StickerColors[3][0] + " | " + StickerColors[3][1] + " |");
        System.out.println("    |----+----+----|----+----+----|----+----+----|----+----+----|");
        System.out.println("    | " + StickerColors[4][6] + " | " + StickerColors[4][8] + " | " + StickerColors[4][2] + " | " + StickerColors[1][6] + " | " + StickerColors[1][8] + " | " + StickerColors[1][2] + " | " + StickerColors[2][6] + " | " + StickerColors[2][8] + " | " + StickerColors[2][2] + " | " + StickerColors[3][6] + " | " + StickerColors[3][8] + " | " + StickerColors[3][2] + " |");
        System.out.println("    |----+----+----|----+----+----|----+----+----|----+----+----|");
        System.out.println("    | " + StickerColors[4][5] + " | " + StickerColors[4][4] + " | " + StickerColors[4][3] + " | " + StickerColors[1][5] + " | " + StickerColors[1][4] + " | " + StickerColors[1][3] + " | " + StickerColors[2][5] + " | " + StickerColors[2][4] + " | " + StickerColors[2][3] + " | " + StickerColors[3][5] + " | " + StickerColors[3][4] + " | " + StickerColors[3][3] + " |");
        System.out.println("    +----+----+----+----+----+----+----+----+----+----+----+----|");
        System.out.println("\t\t   | " + StickerColors[5][7] + " | " + StickerColors[5][0] + " | " + StickerColors[5][1] + " |");
        System.out.println("\t\t   |----+----+----|");
        System.out.println("\t\t   | " + StickerColors[5][6] + " | " + StickerColors[5][8] + " | " + StickerColors[5][2] + " |");
        System.out.println("\t\t   |----+----+----|");
        System.out.println("\t\t   | " + StickerColors[5][5] + " | " + StickerColors[5][4] + " | " + StickerColors[5][3] + " |");
        System.out.println("\t\t   +----+----+----+");
    }
    
}