import java.util.*;
import java.lang.Math;
import java.lang.Character;

public abstract class Moves{
    public static void SwapStickersSameFace(Character[] Face, int FirstSticker, int SecondSticker){
        char temp = Face[FirstSticker];
        Face[FirstSticker] = Face[SecondSticker];
        Face[SecondSticker] = temp;
    }

    public static void SwapStickersDifferentFace(Character[] FirstFace, int FirstSticker, Character[] SecondFace, int SecondSticker){
        char temp = FirstFace[FirstSticker];
        FirstFace[FirstSticker] = SecondFace[SecondSticker];
        SecondFace[SecondSticker] = temp;
    }
    
    public static void SwapFaces(Character[] FirstFace, Character[] SecondFace){
        Character[] temp = new Character[9];
        for (int Stickers = 0; Stickers < 9; Stickers++){
            temp[Stickers] = FirstFace[Stickers];
            FirstFace[Stickers] = SecondFace[Stickers];
            SecondFace[Stickers] = temp[Stickers];
        }
    }

    public static void RotateFaceClockwise(Character[] Face){
        SwapStickersSameFace(Face, 1, 7); SwapStickersSameFace(Face, 7, 5); SwapStickersSameFace(Face, 5, 3);
        SwapStickersSameFace(Face, 0, 6); SwapStickersSameFace(Face, 6, 4); SwapStickersSameFace(Face, 4, 2);
    }

    public static void RotateFaceCounterClockwise(Character[] Face){
        SwapStickersSameFace(Face, 1, 3); SwapStickersSameFace(Face, 3, 5); SwapStickersSameFace(Face, 5, 7);
        SwapStickersSameFace(Face, 0, 2); SwapStickersSameFace(Face, 2, 4); SwapStickersSameFace(Face, 4, 6);
    }

    public static void RotateFaceTwice(Character[] Face){
        SwapStickersSameFace(Face, 1, 5); SwapStickersSameFace(Face, 3, 7);
        SwapStickersSameFace(Face, 0, 4); SwapStickersSameFace(Face, 2, 6);
    }

    public static void RotateClockwise(Rubiks R, Character Face){
        switch (Face){
            case 'U' :
                RotateFaceClockwise(R.UpFace);
                // Swap Corresponding Corners
                SwapStickersDifferentFace(R.BackFace, 7, R.LeftFace, 7); SwapStickersDifferentFace(R.LeftFace, 7, R.FrontFace, 7); SwapStickersDifferentFace(R.FrontFace, 7, R.RightFace, 7);
                SwapStickersDifferentFace(R.RightFace, 1, R.BackFace, 1); SwapStickersDifferentFace(R.BackFace, 1, R.LeftFace, 1); SwapStickersDifferentFace(R.LeftFace, 1, R.FrontFace, 1);
                // Swap Corresponding Edges
                SwapStickersDifferentFace(R.BackFace, 0, R.LeftFace, 0); SwapStickersDifferentFace(R.LeftFace, 0, R.FrontFace, 0); SwapStickersDifferentFace(R.FrontFace, 0, R.RightFace, 0);
                break;
            
            case 'F' :
                RotateFaceClockwise(R.FrontFace);
                // Swap Corresponding Corners
                SwapStickersDifferentFace(R.UpFace, 3, R.LeftFace, 1); SwapStickersDifferentFace(R.LeftFace, 1, R.DownFace, 7); SwapStickersDifferentFace(R.DownFace, 7, R.RightFace, 5);
                SwapStickersDifferentFace(R.RightFace, 7, R.UpFace, 5); SwapStickersDifferentFace(R.UpFace, 5, R.LeftFace, 3); SwapStickersDifferentFace(R.LeftFace, 3, R.DownFace, 1);
                // Swap Corresponding Edges
                SwapStickersDifferentFace(R.UpFace, 4, R.LeftFace, 2); SwapStickersDifferentFace(R.LeftFace, 2, R.DownFace, 0); SwapStickersDifferentFace(R.DownFace, 0, R.RightFace, 6);
                break;
            
            case 'R' :
                RotateFaceClockwise(R.RightFace);
                // Swap Corresponding Corners
                SwapStickersDifferentFace(R.UpFace, 1, R.FrontFace, 1); SwapStickersDifferentFace(R.FrontFace, 1, R.DownFace, 1); SwapStickersDifferentFace(R.DownFace, 1, R.BackFace, 5);
                SwapStickersDifferentFace(R.BackFace, 7, R.UpFace, 3); SwapStickersDifferentFace(R.UpFace, 3, R.FrontFace, 3); SwapStickersDifferentFace(R.FrontFace, 3, R.DownFace, 3);
                // Swap Corresponding Edges
                SwapStickersDifferentFace(R.UpFace, 2, R.FrontFace, 2); SwapStickersDifferentFace(R.FrontFace, 2, R.DownFace, 2); SwapStickersDifferentFace(R.DownFace, 2, R.BackFace, 6);
                break;
            
            case 'B' :
                RotateFaceClockwise(R.BackFace);
                // Swap Corresponding Corners
                SwapStickersDifferentFace(R.UpFace, 7, R.RightFace, 1); SwapStickersDifferentFace(R.RightFace, 1, R.DownFace, 3); SwapStickersDifferentFace(R.DownFace, 3, R.LeftFace, 5);
                SwapStickersDifferentFace(R.LeftFace, 7, R.UpFace, 1); SwapStickersDifferentFace(R.UpFace, 1, R.RightFace, 3); SwapStickersDifferentFace(R.RightFace, 3, R.DownFace, 5);
                // Swap Corresponding Edges
                SwapStickersDifferentFace(R.UpFace, 0, R.RightFace, 2); SwapStickersDifferentFace(R.RightFace, 2, R.DownFace, 4); SwapStickersDifferentFace(R.DownFace, 4, R.LeftFace, 6);
                break;
            
            case 'L' :
                RotateFaceClockwise(R.LeftFace);
                // Swap Corresponding Corners
                SwapStickersDifferentFace(R.UpFace, 5, R.BackFace, 1); SwapStickersDifferentFace(R.BackFace, 1, R.DownFace, 5); SwapStickersDifferentFace(R.DownFace, 5, R.FrontFace, 5);
                SwapStickersDifferentFace(R.FrontFace, 7, R.UpFace, 7); SwapStickersDifferentFace(R.UpFace, 7, R.BackFace, 3); SwapStickersDifferentFace(R.BackFace, 3, R.DownFace, 7);
                // Swap Corresponding Edges
                SwapStickersDifferentFace(R.UpFace, 6, R.BackFace, 2); SwapStickersDifferentFace(R.BackFace, 2, R.DownFace, 6); SwapStickersDifferentFace(R.DownFace, 6, R.FrontFace, 6);
                break;
            
            case 'D' :
                RotateFaceClockwise(R.DownFace);
                // Swap Corresponding Corners
                SwapStickersDifferentFace(R.FrontFace, 3, R.LeftFace, 3); SwapStickersDifferentFace(R.LeftFace, 3, R.BackFace, 3); SwapStickersDifferentFace(R.BackFace, 3, R.RightFace, 3);
                SwapStickersDifferentFace(R.RightFace, 5, R.FrontFace, 5); SwapStickersDifferentFace(R.FrontFace, 5, R.LeftFace, 5); SwapStickersDifferentFace(R.LeftFace, 5, R.BackFace, 5);
                // Swap Corresponding Edges
                SwapStickersDifferentFace(R.FrontFace, 4, R.LeftFace, 4); SwapStickersDifferentFace(R.LeftFace, 4, R.BackFace, 4);SwapStickersDifferentFace(R.BackFace, 4, R.RightFace, 4);
                break;
            default :
                System.out.println("Move invalid!");
        }
    }

    public static void RotateCounterClockwise(Rubiks R, Character Face){
        switch (Face){
            case 'U' :
                RotateFaceCounterClockwise(R.UpFace);
                // Swap Corresponding Corners
                SwapStickersDifferentFace(R.BackFace, 7, R.RightFace, 7); SwapStickersDifferentFace(R.RightFace, 7, R.FrontFace, 7); SwapStickersDifferentFace(R.FrontFace, 7, R.LeftFace, 7);
                SwapStickersDifferentFace(R.RightFace, 1, R.FrontFace, 1); SwapStickersDifferentFace(R.FrontFace, 1, R.LeftFace, 1); SwapStickersDifferentFace(R.LeftFace, 1, R.BackFace, 1);
                // Swap Corresponding Edges
                SwapStickersDifferentFace(R.BackFace, 0, R.RightFace, 0); SwapStickersDifferentFace(R.RightFace, 0, R.FrontFace, 0); SwapStickersDifferentFace(R.FrontFace, 0, R.LeftFace, 0);
                break;
            
            case 'F' :
                RotateFaceCounterClockwise(R.FrontFace);
                // Swap Corresponding Corners
                SwapStickersDifferentFace(R.UpFace, 3, R.RightFace, 5); SwapStickersDifferentFace(R.RightFace, 5, R.DownFace, 7); SwapStickersDifferentFace(R.DownFace, 7, R.LeftFace, 1);
                SwapStickersDifferentFace(R.RightFace, 7, R.DownFace, 1); SwapStickersDifferentFace(R.DownFace, 1, R.LeftFace, 3); SwapStickersDifferentFace(R.LeftFace, 3, R.UpFace, 5);                
                // Swap Corresponding Edges
                SwapStickersDifferentFace(R.UpFace, 4, R.RightFace, 6); SwapStickersDifferentFace(R.RightFace, 6, R.DownFace, 0); SwapStickersDifferentFace(R.DownFace, 0, R.LeftFace, 2);
                break;
            
            case 'R' :
                RotateFaceCounterClockwise(R.RightFace);
                // Swap Corresponding Corners
                SwapStickersDifferentFace(R.UpFace, 1, R.BackFace, 5); SwapStickersDifferentFace(R.BackFace, 5, R.DownFace, 1); SwapStickersDifferentFace(R.DownFace, 1, R.FrontFace, 1);
                SwapStickersDifferentFace(R.BackFace, 7, R.DownFace, 3); SwapStickersDifferentFace(R.DownFace, 3, R.FrontFace, 3); SwapStickersDifferentFace(R.FrontFace, 3, R.UpFace, 3);
                // Swap Corresponding Edges
                SwapStickersDifferentFace(R.UpFace, 2, R.BackFace, 6); SwapStickersDifferentFace(R.BackFace, 6, R.DownFace, 2); SwapStickersDifferentFace(R.DownFace, 2, R.FrontFace, 2);
                break;
            
            case 'B' :
                RotateFaceCounterClockwise(R.BackFace);
                // Swap Corresponding Corners
                SwapStickersDifferentFace(R.UpFace, 7, R.LeftFace, 5); SwapStickersDifferentFace(R.LeftFace, 5, R.DownFace, 3); SwapStickersDifferentFace(R.DownFace, 3, R.RightFace, 1);
                SwapStickersDifferentFace(R.LeftFace, 7, R.DownFace, 5); SwapStickersDifferentFace(R.DownFace, 5, R.RightFace, 3); SwapStickersDifferentFace(R.RightFace, 3, R.UpFace, 1);
                // Swap Corresponding Edges
                SwapStickersDifferentFace(R.UpFace, 0, R.LeftFace, 6); SwapStickersDifferentFace(R.LeftFace, 6, R.DownFace, 4); SwapStickersDifferentFace(R.DownFace, 4, R.RightFace, 2);
                break;
            
            case 'L' :
                RotateFaceCounterClockwise(R.LeftFace);
                // Swap Corresponding Corners
                SwapStickersDifferentFace(R.UpFace, 5, R.FrontFace, 5); SwapStickersDifferentFace(R.FrontFace, 5, R.DownFace, 5); SwapStickersDifferentFace(R.DownFace, 5, R.BackFace, 1);
                SwapStickersDifferentFace(R.FrontFace, 7, R.DownFace, 7); SwapStickersDifferentFace(R.DownFace, 7, R.BackFace, 3);  SwapStickersDifferentFace(R.BackFace, 3, R.UpFace, 7);
                // Swap Corresponding Edges
                SwapStickersDifferentFace(R.UpFace, 6, R.FrontFace, 6); SwapStickersDifferentFace(R.FrontFace, 6, R.DownFace, 6); SwapStickersDifferentFace(R.DownFace, 6, R.BackFace, 2);
                break;
            
            case 'D' :
                RotateFaceCounterClockwise(R.DownFace);
                // Swap Corresponding Corners
                SwapStickersDifferentFace(R.FrontFace, 3, R.RightFace, 3); SwapStickersDifferentFace(R.RightFace, 3, R.BackFace, 3); SwapStickersDifferentFace(R.BackFace, 3, R.LeftFace, 3);
                SwapStickersDifferentFace(R.RightFace, 5, R.BackFace, 5); SwapStickersDifferentFace(R.BackFace, 5, R.LeftFace, 5); SwapStickersDifferentFace(R.LeftFace, 5, R.FrontFace, 5);
                // Swap Corresponding Edges
                SwapStickersDifferentFace(R.FrontFace, 4, R.RightFace, 4); SwapStickersDifferentFace(R.RightFace, 4, R.BackFace, 4); SwapStickersDifferentFace(R.BackFace, 4, R.LeftFace, 4);
                break;
            default :
                System.out.println("Move invalid!");
        }
    }

    public static void RotateTwice(Rubiks R, Character Face){
        switch (Face){
            case 'U' :
                RotateFaceTwice(R.UpFace);
                // Swap Corresponding Corners
                SwapStickersDifferentFace(R.BackFace, 7, R.FrontFace, 7); SwapStickersDifferentFace(R.RightFace, 7, R.LeftFace, 7);
                SwapStickersDifferentFace(R.RightFace, 1, R.LeftFace, 1); SwapStickersDifferentFace(R.FrontFace, 1, R.BackFace, 1);
                // Swap Corresponding Edges
                SwapStickersDifferentFace(R.BackFace, 0, R.FrontFace, 0); SwapStickersDifferentFace(R.RightFace, 0, R.LeftFace, 0);
                break;
            
            case 'F' :
                RotateFaceTwice(R.FrontFace);
                // Swap Corresponding Corners
                SwapStickersDifferentFace(R.UpFace, 3, R.DownFace, 7); SwapStickersDifferentFace(R.RightFace, 5, R.LeftFace, 1);
                SwapStickersDifferentFace(R.RightFace, 7, R.LeftFace, 3); SwapStickersDifferentFace(R.DownFace, 1, R.UpFace, 5);                
                // Swap Corresponding Edges
                SwapStickersDifferentFace(R.UpFace, 4, R.DownFace, 0); SwapStickersDifferentFace(R.RightFace, 6, R.LeftFace, 2);
                break;
            
            case 'R' :
                RotateFaceTwice(R.RightFace);
                // Swap Corresponding Corners
                SwapStickersDifferentFace(R.UpFace, 1, R.DownFace, 1); SwapStickersDifferentFace(R.BackFace, 5, R.FrontFace, 1);
                SwapStickersDifferentFace(R.BackFace, 7, R.FrontFace, 3); SwapStickersDifferentFace(R.DownFace, 3, R.UpFace, 3);
                // Swap Corresponding Edges
                SwapStickersDifferentFace(R.UpFace, 2, R.DownFace, 2); SwapStickersDifferentFace(R.BackFace, 6, R.FrontFace, 2);
                break;
            
            case 'B' :
                RotateFaceTwice(R.BackFace);
                // Swap Corresponding Corners
                SwapStickersDifferentFace(R.UpFace, 7, R.DownFace, 3); SwapStickersDifferentFace(R.LeftFace, 5, R.RightFace, 1);
                SwapStickersDifferentFace(R.LeftFace, 7, R.RightFace, 3); SwapStickersDifferentFace(R.DownFace, 5, R.UpFace, 1);
                // Swap Corresponding Edges
                SwapStickersDifferentFace(R.UpFace, 0, R.DownFace, 4); SwapStickersDifferentFace(R.LeftFace, 6, R.RightFace, 2);
                break;
            
            case 'L' :
                RotateFaceTwice(R.LeftFace);
                // Swap Corresponding Corners
                SwapStickersDifferentFace(R.UpFace, 5, R.DownFace, 5); SwapStickersDifferentFace(R.FrontFace, 5, R.BackFace, 1);
                SwapStickersDifferentFace(R.FrontFace, 7, R.BackFace, 3); SwapStickersDifferentFace(R.DownFace, 7, R.UpFace, 7);
                // Swap Corresponding Edges
                SwapStickersDifferentFace(R.UpFace, 6, R.DownFace, 6); SwapStickersDifferentFace(R.FrontFace, 6, R.BackFace, 2);
                break;
            
            case 'D' :
                RotateFaceTwice(R.DownFace);
                // Swap Corresponding Corners
                SwapStickersDifferentFace(R.FrontFace, 3, R.BackFace, 3); SwapStickersDifferentFace(R.RightFace, 3, R.LeftFace, 3);
                SwapStickersDifferentFace(R.RightFace, 5, R.LeftFace, 5); SwapStickersDifferentFace(R.BackFace, 5, R.FrontFace, 5);
                // Swap Corresponding Edges
                SwapStickersDifferentFace(R.FrontFace, 4, R.BackFace, 4); SwapStickersDifferentFace(R.RightFace, 4, R.LeftFace, 4);
                break;
            default :
                System.out.println("Move invalid!");
        }
    } 

    public static void RotateWholeCubeClockwise(Rubiks R, Character Axis){
        switch (Axis){
            case 'x' :
                SwapFaces(R.UpFace, R.FrontFace);
                SwapFaces(R.FrontFace, R.DownFace);
                SwapFaces(R.DownFace, R.BackFace);
                RotateFaceClockwise(R.RightFace);
                RotateFaceTwice(R.BackFace);
                RotateFaceCounterClockwise(R.LeftFace);
                RotateFaceTwice(R.DownFace);
                break;
            case 'y' :
                SwapFaces(R.BackFace, R.LeftFace);
                SwapFaces(R.LeftFace, R.FrontFace);
                SwapFaces(R.FrontFace, R.RightFace);
                RotateFaceClockwise(R.UpFace);
                RotateFaceCounterClockwise(R.DownFace);
                break;
            case 'z' :
                SwapFaces(R.UpFace, R.LeftFace);
                SwapFaces(R.LeftFace, R.DownFace);
                SwapFaces(R.DownFace, R.RightFace);
                RotateFaceClockwise(R.UpFace);
                RotateFaceClockwise(R.FrontFace);
                RotateFaceClockwise(R.RightFace);
                RotateFaceCounterClockwise(R.BackFace);
                RotateFaceClockwise(R.DownFace);
                break;
            default :
                System.out.println("Move invalid!");
        }
    }

    public static void RotateWholeCubeCounterClockwise(Rubiks R, Character Axis){
        switch (Axis){
            case 'x' :
                SwapFaces(R.UpFace, R.BackFace);
                SwapFaces(R.BackFace, R.DownFace);
                SwapFaces(R.DownFace, R.FrontFace);
                RotateFaceTwice(R.UpFace);
                RotateFaceCounterClockwise(R.RightFace);
                RotateFaceTwice(R.BackFace);
                RotateFaceClockwise(R.LeftFace);
                break;
            case 'y' :
                SwapFaces(R.BackFace, R.RightFace);
                SwapFaces(R.RightFace, R.FrontFace);
                SwapFaces(R.FrontFace, R.LeftFace);
                RotateFaceCounterClockwise(R.UpFace);
                RotateFaceClockwise(R.DownFace);
                break;
            case 'z' :
                SwapFaces(R.UpFace, R.RightFace);
                SwapFaces(R.RightFace, R.DownFace);
                SwapFaces(R.DownFace, R.LeftFace);
                RotateFaceCounterClockwise(R.UpFace);
                RotateFaceCounterClockwise(R.FrontFace);
                RotateFaceCounterClockwise(R.RightFace);
                RotateFaceClockwise(R.BackFace);
                RotateFaceCounterClockwise(R.LeftFace);
                RotateFaceCounterClockwise(R.DownFace);
                break;
            default :
                System.out.println("Move invalid!");
        }
    }

    public static void RotateWholeCubeTwice(Rubiks R, Character Axis){
        switch (Axis){
            case 'x' :
                SwapFaces(R.UpFace, R.DownFace);
                SwapFaces(R.FrontFace, R.BackFace);
                RotateFaceTwice(R.FrontFace);
                RotateFaceTwice(R.RightFace);
                RotateFaceTwice(R.BackFace);
                RotateFaceTwice(R.LeftFace);
                break;
            case 'y' :
                SwapFaces(R.FrontFace, R.BackFace);
                SwapFaces(R.RightFace, R.LeftFace);
                RotateFaceTwice(R.UpFace);
                RotateFaceTwice(R.DownFace);
                break;
            case 'z' :
                SwapFaces(R.UpFace, R.RightFace);
                SwapFaces(R.RightFace, R.DownFace);
                SwapFaces(R.DownFace, R.LeftFace);
                RotateFaceTwice(R.UpFace);
                RotateFaceTwice(R.FrontFace);
                RotateFaceTwice(R.RightFace);
                RotateFaceTwice(R.BackFace);
                RotateFaceTwice(R.LeftFace);
                RotateFaceTwice(R.BackFace);
                break;
            default :
                System.out.println("Move invalid!");
        }
    }
    
    public static void Move(Rubiks R, String Notation){
        switch (Notation.length()){
            case 1 :
                if (Character.isLowerCase(Notation.charAt(0))){
                    RotateWholeCubeClockwise(R, Notation.charAt(0));
                }
                else{
                    RotateClockwise(R, Notation.charAt(0));
                }
                break;
            case 2 :
                if (Character.isLowerCase(Notation.charAt(0))){
                    if (Notation.charAt(1) == '2'){
                        RotateWholeCubeTwice(R, Notation.charAt(0));
                    }
                    else if (Notation.charAt(1) == '\''){
                        RotateWholeCubeCounterClockwise(R, Notation.charAt(0));
                    }
                }
                else if (!Character.isLowerCase(Notation.charAt(0))){
                    if (Notation.charAt(1) == '2'){
                        RotateTwice(R, Notation.charAt(0));
                    }
                    else if (Notation.charAt(1) == '\''){
                        RotateCounterClockwise(R, Notation.charAt(0));
                    }
                }
                else{
                    System.out.println("Move invalid!");
                }
                break;
            default :
                System.out.println("Move invalid!");
        }
    }
}