import java.util.*;

public class Main{
    public static void main(String[] args){
        
        Rubiks R = new Rubiks();
        Utils.InputColorsToFaces(R);
        Utils.DisplayCubeNet(R);
        String Menu = "main";
        Scanner MenuInput = new Scanner(System.in);

        while (!Menu.equals("quit")){
            System.out.printf("Menu : ");
            Menu = MenuInput.next();
            try{
                Moves.Move(R, Menu);
                Utils.DisplayCubeNet(R);
            }
            catch (Exception e){
                System.out.println("Input invalid!");
            }
            
        }
        MenuInput.close();
    }
}