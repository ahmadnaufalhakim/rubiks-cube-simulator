import java.util.*;

public class Rubiks{
    
    // Representing each face with an array of 9 chars
    Character[] UpFace      = new Character[9];
    Character[] FrontFace   = new Character[9];
    Character[] RightFace   = new Character[9];
    Character[] BackFace    = new Character[9];
    Character[] LeftFace    = new Character[9];
    Character[] DownFace    = new Character[9];

    // Chars as Colors
    static final Character YELLOW   = 'y';
    static final Character RED      = 'r';
    static final Character GREEN    = 'g';
    static final Character ORANGE   = 'o';
    static final Character BLUE     = 'b';
    static final Character WHITE    = 'w';

    // Default Constructor
    public Rubiks(){
        Arrays.fill(UpFace, YELLOW);
        Arrays.fill(FrontFace, RED);
        Arrays.fill(RightFace, GREEN);
        Arrays.fill(BackFace, ORANGE);
        Arrays.fill(LeftFace, BLUE);
        Arrays.fill(DownFace, WHITE);
    }
}